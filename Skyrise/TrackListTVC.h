//
//  TrackListTVC.h
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackListTVC : UITableViewController
{
    BOOL connectionFailed;
    NSString *currentArtist;
    NSArray *tracksList;
}

-(IBAction)fetchData:(_Nullable id)sender;
+(NSString * _Nonnull) randomizeArtist;

@end

