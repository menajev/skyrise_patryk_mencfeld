//
//  TrackListTVC.m
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrackListTVC.h"
#import "ConnectionManager.h"
#import "TrackCell.h"
#import "TrackDetailVC.h"

@interface TrackListTVC ()

@end

@implementation TrackListTVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentArtist = [TrackListTVC randomizeArtist];
    self.title = currentArtist;
    
    connectionFailed = NO;
    [self fetchData:nil];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Table 

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tracksList)
        return tracksList.count;
    
    return 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"TrackListCellID";
    static NSString *waitingCellID = @"WaitingCell";
    static NSString *errorCellID = @"ErrorCell";
    
    if (tracksList.count == 0) {
        UITableViewCell *cell;
        if (connectionFailed) {
            cell = [tableView dequeueReusableCellWithIdentifier:errorCellID];
        }
        else {
            cell = [tableView dequeueReusableCellWithIdentifier:waitingCellID];
            
            [(UIActivityIndicatorView*)[cell.contentView viewWithTag:10] startAnimating];
        }
        
        return cell;
    }
    
    TrackCell *cell= (TrackCell*)[tableView dequeueReusableCellWithIdentifier:cellID];
    
    NSDictionary *trackInfo = tracksList[indexPath.row];
    
    cell.trackName.text = trackInfo[@"trackName"];
    cell.trackAlbumName.text = trackInfo[@"collectionName"];
    
    int size = 60;
    if ( [UIScreen mainScreen].scale >= 2)
        size = 100;
    
    NSString *trackCoverUrl = trackInfo[[NSString stringWithFormat:@"artworkUrl%i", size]];
    cell.trackCoverUrl = trackCoverUrl;
        
    return cell;
}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.destinationViewController isKindOfClass:[TrackDetailVC class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        ((TrackDetailVC*)segue.destinationViewController).trackInfo = tracksList[indexPath.row];
    }
}

#pragma mark -

+(NSString *) randomizeArtist {
    NSString *artistsListsBundle= [[NSBundle mainBundle] pathForResource:@"ArtistsList" ofType:@"plist"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *artistsListsPath = [basePath stringByAppendingPathComponent:@"ArtistsList.plist"];
    
    NSMutableArray *list = [NSMutableArray arrayWithContentsOfFile:artistsListsPath];
    if (list == nil || list.count == 0)
        list = [NSMutableArray arrayWithContentsOfFile:artistsListsBundle];
    
    int no = arc4random() % list.count;
    
    NSString *currentArist = list[no];
    
    [list removeObject:currentArist];
    
    [list writeToFile:artistsListsPath atomically:YES];
    
    return currentArist;
}

#pragma mark -

-(IBAction)fetchData:(id)sender {
    if (sender != nil) {
        connectionFailed = NO;
        [self.tableView reloadData];
    }
    
    [[ConnectionManager sharedConnectionManager] downloadTracksForArist:currentArtist completionBlock:^(NSArray *tracks) {
        if (tracks) {
            tracksList = [tracks sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                
                if ([obj1[@"collectionName"] isEqualToString:obj2[@"collectionName"]])
                    return [obj1[@"trackNumber"] compare:obj2[@"trackNumber"]];
                
                return [obj1[@"collectionName"] compare:obj2[@"collectionName"]];
                
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
            });
        }
        else {
            connectionFailed = YES;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                // delay reloading to allow user to see "downloading" cell even if request was handled immidiately
                [self.tableView reloadData];
            });
        }
    }];
}

@end
