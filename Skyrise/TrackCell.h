//
//  TrackCell.h
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h> 

@interface TrackCell : UITableViewCell 

@property (strong) IBOutlet UIImageView *trackCover;

@property (strong) IBOutlet UILabel *trackName;
@property (strong) IBOutlet UILabel *trackAlbumName;

@property (strong, nonatomic) NSString *trackCoverUrl;

@end
