//
//  ConnectionManager.h
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ConnectionManager : NSObject
{
    NSMutableArray *requestsInProgress;
}

+(ConnectionManager* _Nonnull) sharedConnectionManager;

-(void) downloadTracksForArist:(NSString* _Nonnull) artistName completionBlock:( void (^ _Nonnull )(NSArray * _Nullable tracks)) completionBlock;
-(void) downloadImageWithUrl:(NSString* _Nonnull) url;

@end
