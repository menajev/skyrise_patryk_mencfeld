//
//  ConnectionManager.m
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager

+(ConnectionManager*) sharedConnectionManager
{
    static dispatch_once_t once;
    static ConnectionManager *shared;
    
    dispatch_once(&once, ^{
        if (shared == nil)
            shared = [[ConnectionManager alloc] init];
    });
    
    return shared;
}

-(id) init {
    self = [super init];
    
    requestsInProgress = [[NSMutableArray alloc] init];
    
    return self;
}

-(void) downloadTracksForArist:(NSString*) artistName completionBlock:(void (^)(NSArray *))completionBlock {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/search?term=%@&entity=musicTrack", 
                                       [artistName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (data && error == nil) {
        NSError *jsonError;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingAllowFragments
                                                                   error:&jsonError];
            
            if (dict && jsonError == nil) {
                NSArray *tracks = dict[@"results"];
                completionBlock(tracks);
            }
            else {
                completionBlock(nil);
            }
        }
        else {
            completionBlock(nil);
        }
    }];
    [dataTask resume];
}

-(void) downloadImageWithUrl:(NSString *)imageUrl {
    if ([requestsInProgress containsObject:imageUrl])
        return;
    
    [requestsInProgress addObject:imageUrl];
    
    NSURL *url = [NSURL URLWithString:[imageUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data && error == nil) {
            UIImage *img = [[UIImage alloc] initWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [requestsInProgress removeObject:imageUrl];
                [[NSNotificationCenter defaultCenter] postNotificationName:imageUrl object:img];
            });
        }else{
            [requestsInProgress removeObject:imageUrl];
            // TODO: retry
        }
    }];
    [dataTask resume];

    
}
@end
