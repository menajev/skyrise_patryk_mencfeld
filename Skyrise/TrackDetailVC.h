//
//  TrackDetailVC.h
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackDetailVC : UIViewController
{
    IBOutlet UILabel *aristName;
    IBOutlet UILabel *trackTitle;
    IBOutlet UILabel *albumName;
    IBOutlet UILabel *trackNumber;
    IBOutlet UILabel *releaseData;
}

@property (strong) NSDictionary *trackInfo;

@end
