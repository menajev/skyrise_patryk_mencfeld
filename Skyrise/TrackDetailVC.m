//
//  TrackDetailVC.m
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrackDetailVC.h"

@interface TrackDetailVC ()

@end

@implementation TrackDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    trackTitle.text = _trackInfo[@"trackName"];
    self.title = _trackInfo[@"artistName"];
    albumName.text = _trackInfo[@"collectionName"];
    
    if (_trackInfo[@"trackNumber"])
        trackNumber.text = [NSString stringWithFormat:@"%@ / %@", _trackInfo[@"trackNumber"], _trackInfo[@"trackCount"]];
    else
        trackNumber.hidden = YES;
    
    self.navigationItem.backBarButtonItem.title = @"";
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *date = [df dateFromString:_trackInfo[@"releaseDate"]];
    [df setDateFormat:@"dd-MM-yyyy"];
    releaseData.text = [df stringFromDate:date];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
