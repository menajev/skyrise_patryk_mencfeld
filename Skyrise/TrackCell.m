//
//  TrackCell.m
//  Skyrise
//
//  Created by Admin on 07/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "TrackCell.h"
#import "ConnectionManager.h"

@implementation TrackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setTrackCoverUrl:(NSString *)trackCoverUrl {
    _trackCoverUrl = trackCoverUrl;
    _trackCover.image = nil; // TODO: blank image
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCover:) name:trackCoverUrl object:nil];
    
    [[ConnectionManager sharedConnectionManager] downloadImageWithUrl:trackCoverUrl];
}

-(void) changeCover:(NSNotification *)notif {
    _trackCover.image = notif.object;
}

@end
