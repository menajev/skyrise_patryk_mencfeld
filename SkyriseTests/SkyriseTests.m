//
//  SkyriseTests.m
//  SkyriseTests
//
//  Created by Admin on 09/09/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TrackListTVC.h"

@interface SkyriseTests : XCTestCase

@end

@implementation SkyriseTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRandomizeArtist {
    NSString *artistsListsBundle= [[NSBundle mainBundle] pathForResource:@"ArtistsList" ofType:@"plist"];
    NSInteger artistsCount = [[NSMutableArray arrayWithContentsOfFile:artistsListsBundle] count];
    
    for (int i = 0; i < artistsCount + 1; i++) {
        NSString *artist = [TrackListTVC randomizeArtist];
        XCTAssertNotNil(artist, @"Failed to return artist");
    }
}

@end
